# train models

library(Rnumerai)


# 6 Feb 2021 --------------------------------------------------------------

feb6 <- read_csv("data/6 Feb 2021/numerai_training_data.csv")

feb6test <- read_csv("data/6 Feb 2021/numerai_tournament_data.csv")

datNames <- data.frame(names(feb6))

cb <- codebook(feb6)

cb2 <- codebook(feb6)

codebook_fun <- map_df(df, function(x) attributes(x)$label)

covs <- names(feb6[,4:313])

vars <- paste(covs, collapse="+")

form <- as.formula(
  paste("target", "~", vars)
)

form

out <- ls()

for(i in covs) {
  tally(i)
}

covs2 <- feb6[,3:313]

l1 <- lm(target ~ . - id - era - data_type,
         data=feb6)


l1.1 <- lm(target ~ as.formula(vars),
           data=feb6)

l.3 <- lm(form, data=feb6)

l.3_pred <- predict(l.3,
                    feb6test)
head(l.3_pred)

l.3_mse <- (feb6test$target - l.3_pred)^2
head(l.3_mse)
mean(l.3_mse, na.rm=T)

feb621_pred <- data.frame(id=feb6test$id,
                          prediction=l.3_pred)

head(feb621_pred)

describe(feb6$target)
describe(feb6test$target)
describe(feb621_pred$prediction)

write_csv(feb621_pred, "data/6 Feb 2021/6Feb2021_predictions.csv")



# session 253 (27 Feb 2021) -----------------------------------------------


# session 254 (7 March 2021) ----------------------------------------------


set_public_id("3YFUDFPPGVGDK6UD7FXIG4MJXMQ6T47G")
set_api_key("2MEOKUJFA77CPNDRYYMTIYRAZ5GORCXQM43RXOVOMPLYKJKTFGH67P56QQNZMPQS")

dat <- download_data("data/train")

train <- dat$data_train
names(train)

test <- dat$data_tournament
names(test)

rm(dat)

#vars <- paste(covs, collapse="+")

form <- as.formula(
  paste("target", "~",paste(names(train[,4:313]), collapse="+")))
form

form2 <- as.formula(
  paste("target", "~",paste(names(train[,4:313]), collapse="+"), "+ (1|era)"))
form2


form2 <- paste(form, "+", "(1|era)")
form2

l1 <- lm(form,
         data=train)

l1_pred <- predict(l1,
                   newdata=test)

head(l1_pred)

l1_rmse <- sqrt((l1_pred - test$target)^2)

l1_rmse
describe(l1_rmse)


frq(train$era)


lmer1 <- lmer(form2,
              data=train)

lmer1_pred <- predict(lmer1,
                      allow.new.levels=T,
                      newdata=test)

head(lmer1_pred)

lmer1_rmse <- sqrt((lmer1_pred - test$target)^2)

lmer1_rmse

describe(lmer1_rmse)

fa.parallel(dat[,4:313])



l1_submission <- data.frame(id = test$id,
                            prediction=l1_pred)

head(l1_submission)

write_csv(l1_submission, "data/train/27Feb2021/numerai_27Feb2021_submission.csv")

submission_id <- submit_predictions(l1_submission, "data/train/27Feb2021")

?submit_predictions

sub_id <- data.frame(submission_id = submission_id)

write_csv(sub_id, "data/train/27Feb2021/27Feb2021_sub_id.csv")






# session 255 (12 March 2021) ---------------------------------------------


source("code/analysis prep.R")

l1 <- lm(form,
         data=dat)

l1_pred <- predict(l1,
                   newdata=test)

head(l1_pred)

l1_rmse <- sqrt((l1_pred - test$target)^2)

l1_rmse
describe(l1_rmse)
mean(l1_rmse, na.rm=T)

frq(train$era)


lmer1 <- lmer(form2,
              data=dat)

lmer1_pred <- predict(lmer1,
                      allow.new.levels=T,
                      newdata=test)

head(lmer1_pred)

lmer1_rmse <- sqrt((lmer1_pred - test$target)^2)

mean(lmer1_rmse, na.rm=T)

describe(lmer1_rmse)

fa.parallel(dat[,4:313])

fa.parallel(dat_charism)

charism_29 <- fa(dat_charism,
                 nfactors=29,
                 fm="ml",
                 scores="tenBerge")

charism_29
names(dat[,1:5])

charism_29_sc <- data.frame(charism_29$scores) %>%
  mutate(#id = dat$id,
         era=dat$era,
         target=dat$target) %>%
  select(target, era, everything())

head(charism_29_sc[,1:5])

frq(dat$target)

charism1 <- lm(target ~ .,
              data=charism_29_sc)


charism1_inpred <- data.frame(actual=dat$target,
                              predicted=predict(charism1)) %>%
  mutate(error = (actual-predicted)^2)

head(charism1_inpred)

charism1_sum <- data.frame(model="Charisma, 29 components",
                           in_sample=mean(charism1_inpred$error, na.rm=T))

charism1_sum


test %>%
  select(.,contains("charisma")) %>%
  fa.parallel



test_charism <- test %>%
  select(target, contains("charism"))

train_charism <- train %>%
  select(target, contains("charism"))

frq(train_charism$era)
frq(test_charism$era)

?lm_robust

charism_rob <- lm(target ~ .,
                  fixed_effects="era",
                  data=train_charism)

charism_rob_pred <- data.frame(actual=test_charism$target,
                              predicted=predict(charism_rob,
                                                newdata=test_charism)) %>%
  mutate(error = (actual-predicted)^2)

head(charism1_inpred)

charism1_sum <- data.frame(model="Charisma, 29 components",
                           in_sample=mean(charism1_inpred$error, na.rm=T))

charism1_sum



charism_lm <- lm(target ~ .,
                 data=train_charism)

charism_lm_inpred <- data.frame(train_actual=train_charism$target,
                              in_predicted=predict(charism_lm)) %>%
  mutate(in_error = (train_actual-in_predicted)^2)

head(charism_lm_inpred)


charism_lm_outpred <- data.frame(test_actual=test_charism$target,
                                out_predicted=predict(charism_lm,
                                                      newdata=test_charism)) %>%
  mutate(out_error = (test_actual-out_predicted)^2)

head(charism_lm_outpred)




charism_lm_sum <- data.frame(model="Charisma, linear",
                           in_sample=mean(charism_lm_inpred$in_error, na.rm=T),
                           out_sample=mean(charism_lm_outpred$out_error, na.rm=T)) %>%
  mutate(diff_prcnt = ((out_sample - in_sample) / in_sample)*100)

charism_lm_sum


str(test_charism)

charism_lm_test <- lm(target ~ .,
                 data=test_charism)

charism_lm_test_inpred <- data.frame(actual=test_charism$target,
                                predicted=predict(charism_lm_test,
                                                  newdata=test_charism,
                                                  allow.new.levels=T)) %>%
  mutate(in_error = (actual-predicted)^2)

head(charism_lm_test_inpred)

mean(charism_lm_test_inpred$in_error, na.rm=T)




head(lmer1_pred)

lmer1_rmse <- sqrt((lmer1_pred - test$target)^2)

mean(lmer1_rmse, na.rm=T)



train_wis <- train %>%
  select(target, contains("wisdom"))

test_wis <- test %>%
  select(target, contains("wisdom"))


wisdom_lm <- lm(target ~ .,
                 data=train_wis)

wisdom_lm_inpred <- data.frame(train_actual=train_wis$target,
                                in_predicted=predict(wisdom_lm)) %>%
  mutate(in_error = (train_actual-in_predicted)^2)

head(wisdom_lm_inpred)


wisdom_lm_outpred <- data.frame(test_actual=test_wis$target,
                                 out_predicted=predict(wisdom_lm,
                                                       newdata=test_wis)) %>%
  mutate(out_error = (test_actual-out_predicted)^2)

head(wisdom_lm_outpred)


wisdom_lm_sum <- data.frame(model="Wisdom, linear",
                             in_sample=mean(wisdom_lm_inpred$in_error, na.rm=T),
                             out_sample=mean(wisdom_lm_outpred$out_error, na.rm=T)) %>%
  mutate(diff_prcnt = ((out_sample - in_sample) / in_sample)*100)

wisdom_lm_sum

describe(test$target)

names(test_wis)
describe(test_wis)
frq(test$target)

wis_reth <- alist(
  target ~ dnorm( mu , sigma ),
  mu ~ dnorm(.5, .22),
  sigma ~ dexp( 1 )
)



wis_submission <- data.frame(id = test$id,
                            prediction=wisdom_lm_outpred$out_predicted)

head(wis_submission)

write_csv(wis_submission, "data/255/wisdom_submission_12Mar21.csv")

wis_submission <- read_csv("data/255/wisdom_submission_12Mar21.csv")


submission_id <- submit_predictions(wis_submission, "data/255")

?submit_predictions

sub_id <- data.frame(submission_id = submission_id)

write_csv(sub_id, "data/12Mar21_sub_id.csv")

sub_ids <- data.frame(date="2021-03-12", sub_id=sub_id)

sub_ids

write_csv(sub_ids, "data/sub_ids.csv")
